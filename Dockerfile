FROM node:12-alpine

WORKDIR /var/www
COPY . .


RUN npm install
RUN npm run build

EXPOSE 3000
CMD npm start