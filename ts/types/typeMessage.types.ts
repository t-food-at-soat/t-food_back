enum TypeMessage {
    MAIL = "mail",
    SMS = "sms",
    NOTIFICATION = "notification"
}

export default TypeMessage;