enum OfferTarget {
   association = "Associations",
   individual = "Particuliers"
}

export default OfferTarget;