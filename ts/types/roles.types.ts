enum Roles {
  ADMIN = "admin",
  INDIVIDUAL = "particulier",
  CORPORATION = "entreprise",
  ASSOCIATION = "association",
  PUBLIC = "public"
}

export default Roles;