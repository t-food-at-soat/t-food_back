import Offer from "./offer.interfaces";
import Point from "../types/point.types";
import Roles from "../types/roles.types";

export default class User {
  id: number | undefined;
  name: string | undefined;
  email: string | undefined;
  phone: string | undefined;
  address: string | undefined;
  addressCoord: [number,number] | undefined;
  picture: string | undefined;
  history: [Offer] | undefined;
  password: string | undefined;
  roles: Roles | undefined;

  constructor(
    name?: string,
    email?: string,
    password?: string,
    phone?: string,
    address?: string,
    roles?: Roles,
    id?: number,
    addressCoord?: [number, number],
    picture?: string,
    history?: [Offer]
  ) {
    this.id = id;
    this.name = name;
    this.email = email;
    this.phone = phone;
    this.password = password;
    this.address = address;
    this.addressCoord = addressCoord;
    this.picture = picture;
    this.history = history;
    this.roles = roles;
  }
}
