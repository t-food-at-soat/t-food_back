import User from "./user.interfaces";
import Offer from "./offer.interfaces";
import ReservationStatus from "../types/reservationStatus.types";

export default class Reservation {

    id : number
    user : User
    offer : Offer
    status: ReservationStatus
    datetime : Date

    constructor(user ? : User, offer ? : Offer, datetime ?: Date){
        this.user = user;
        this.offer = offer;
        this.status = ReservationStatus.Passed;
        this.datetime = datetime;
    }

    public setStatus(status: ReservationStatus) {
        this.status = status;
    }
}