import Roles from "../types/roles.types";

export default class ResponseRequest{  
    statusCode : number
    message : string
    sqlMessage : string
    status : string
    data : any
    insertedId : number

    constructor(status : string){
        this.status = status;
        this.statusCode = 200;
    }

    setStatusCode(code : number) {
        this.statusCode = code;
        return this;
    }
    setInsertedId(insertedId : number) {
        this.insertedId = insertedId;
        return this;
    }

    setData(data : object){
        this.data = data;
        return this;
    }

    setMessage(message: string) {
        this.message = message;
        return this;
    }
    setSqlMessage(message: string) {
        this.sqlMessage = message;
        return this;
    }
}