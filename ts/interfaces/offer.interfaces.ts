import Status from '../types/status.types'
import OfferType from '../types/offerType.types';
import Point from '../types/point.types';
import OfferTarget from '../types/offerTarget.types';
import User from './user.interfaces';

export default class Offer {
   id : number 
   description : string
   status : Status
   datetime : Date
   type : OfferType
   target : OfferTarget
   quantity : number
   location : string
   picture : string
   author: User

   constructor(
      id ?: number,
      description ?: string,
      datetime ?: Date,
      type ?: OfferType,
      target ?: OfferTarget,
      quantity ?: number,
      location ?: string,
      picture ?: string,
      author ?: User
      ){
      this.id = id;
      this.description = description;
      this.status = Status.Opened;
      this.datetime = datetime;
      this.type = type;
      this.target = target;
      this.quantity = quantity;
      this.location = location;
      this.picture = picture;
      this.author = author;
   }


   public setStatus(status : Status) : void{
      this.status = status;
   }
}
