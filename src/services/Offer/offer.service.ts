import Service from "../../utils/service";
import HTTP_METHODS from "../../../ts/types/httpMethod.types";
import Roles from "../../../ts/types/roles.types";
import { getOfferSchema, postUserSchema } from "./offer.schema";
import OfferRepository from "../../repositories/offer.repository";
import OfferTarget from "../../../ts/types/offerTarget.types";
import { Response, Request } from "express";
import Offer from "../../../ts/interfaces/offer.interfaces";

export default class OfferService extends Service {
  initRoute() {
    this.routesConfig = [
      {
        route: "/offers",
        execute: "getOffer",
        method: HTTP_METHODS.GET,
        schema: getOfferSchema,
        roles: [Roles.ADMIN, Roles.ASSOCIATION, Roles.INDIVIDUAL]
      },
      {
        route: "/findAllOrdersByAuthor/:userName",
        execute: "findAllOrdersByAuthor",
        method: HTTP_METHODS.GET,
        schema: getOfferSchema,
        roles: [
          Roles.ADMIN,
          Roles.ASSOCIATION,
          Roles.INDIVIDUAL,
          Roles.CORPORATION
        ]
      },
      {
        route: "/findOfferById/:offerId",
        execute: "findOfferById",
        method: HTTP_METHODS.GET,
        schema: getOfferSchema,
        roles: [Roles.ADMIN, Roles.ASSOCIATION, Roles.INDIVIDUAL]
      },
      {
        route: "/findAllOrdersSortedByNewest",
        execute: "findAllOrdersSortedByNewest",
        method: HTTP_METHODS.GET,
        schema: getOfferSchema,
        roles: [Roles.ADMIN, Roles.ASSOCIATION, Roles.INDIVIDUAL]
      },
      {
        route: "/findAllOrdersSortedByOldest",
        execute: "findAllOrdersSortedByOldest",
        method: HTTP_METHODS.GET,
        schema: getOfferSchema,
        roles: [Roles.ADMIN, Roles.ASSOCIATION, Roles.INDIVIDUAL]
      },
      {
        route: "/offers/admin",
        execute: "getOffer",
        method: HTTP_METHODS.GET,
        schema: getOfferSchema,
        roles: [Roles.ADMIN]
      },
      {
        route: "/createOffer",
        execute: "addOffer",
        method: HTTP_METHODS.POST,
        schema: postUserSchema,
        roles: [Roles.ADMIN, Roles.CORPORATION]
      },
      {
        route: "/updateOffer",
        execute: "updateOffer",
        method: HTTP_METHODS.PUT,
        schema: postUserSchema,
        roles: [Roles.ADMIN, Roles.CORPORATION]
      }
    ];
  }
  getAccordingTarget = (role: Roles) => {
    return role == Roles.ASSOCIATION
      ? OfferTarget.association
      : OfferTarget.individual;
  };
  async getOffer(req: Request & any, res: Response) {
    const result = await OfferRepository.findAllOrdersByTargets(
      this.getAccordingTarget(req.decoded.roles)
    );
    res.status(result.statusCode).send(result);
  }

  async findAllOrdersSortedByNewest(req: Request, res: Response) {
    const result = await OfferRepository.findAllOrdersSortedByNewest(
      OfferTarget.individual
    );
    res.status(result.statusCode).send(result);
  }
  async findAllOrdersSortedByOldest(req: Request, res: Response) {
    const result = await OfferRepository.findAllOrdersSortedByOldest(
      OfferTarget.individual
    );
    res.status(result.statusCode).send(result);
  }

  async findAllOrdersByAuthor(req: Request, res: Response) {
    const result = await OfferRepository.findAllOrdersByAuthor(
      req.params.userName
    );
    res.status(result.statusCode).send(result);
  }

  async findOfferById(req: Request, res: Response) {
    const result = await OfferRepository.findOfferById(
      parseInt(req.params.offerId)
    );
    res.status(result.statusCode).send(result);
  }

  async addOffer(req: Request, res: Response) {
    let offer = Object.assign(new Offer(), req.body);
    const result = await OfferRepository.addOffer(offer);

    res.status(result.statusCode).send(result);
  }

  async updateOffer(req: Request, res: Response) {
    const result = await OfferRepository.updateOffer(
      Object.assign(new Offer(), req.body)
    );
    res.status(result.statusCode).send(result);
  }
}
