const { celebrate, Joi } = require('celebrate');

  const getOfferSchema = celebrate({
  query: Joi.object().keys({
    id: Joi.number(),
    description: Joi.string()
  })
}); 
const postUserSchema = celebrate({
  body: Joi.object().keys({
    id: Joi.number(),
    description: Joi.string(),
    datetime: Joi.string(),
    type: Joi.string(),
    target: Joi.string(),
    quantity: Joi.number(),
    picture: Joi.string(),
    location: Joi.string(),
    author: Joi.object()
  })
});

export {
  getOfferSchema,
  postUserSchema
};