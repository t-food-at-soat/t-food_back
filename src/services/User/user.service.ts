import Service from "../../utils/service";
import HTTP_METHODS from "../../../ts/types/httpMethod.types";
import Roles from "../../../ts/types/roles.types";
import {
  getProfileInfoByIdSchema,
  getProfileInfoByEmailSchema,
  editProfileInfoSchema,
  registerUserSchema,
  getAllUsersSchema
} from "./user.schema";
import UserRepository from "../../repositories/user.repository";
import { Response, Request } from "express";
import User from "../../../ts/interfaces/user.interfaces";
import Password from "../../utils/password";
import Point from "../../../ts/types/point.types";
import JWT_CONFIG from "../../config/jwt.config";

export default class UserService extends Service {
  initRoute() {
    this.routesConfig = [
      {
        route: "/register",
        execute: "registerUser",
        method: HTTP_METHODS.POST,
        schema: registerUserSchema,
        roles: [Roles.PUBLIC]
      },
      {
        route: "/profileInfo/:id",
        execute: "getProfileInfoById",
        method: HTTP_METHODS.GET,
        schema: getProfileInfoByIdSchema,
        roles: [Roles.CORPORATION, Roles.ASSOCIATION, Roles.INDIVIDUAL]
      },
      {
        route: "/profileInfoByEmail/:email",
        execute: "getProfileInfoByEmail",
        method: HTTP_METHODS.GET,
        schema: getProfileInfoByEmailSchema,
        roles: [Roles.CORPORATION, Roles.ASSOCIATION, Roles.INDIVIDUAL]
      },
      {
        route: "/users",
        execute: "getAllUsers",
        method: HTTP_METHODS.GET,
        schema: getAllUsersSchema,
        roles: [Roles.CORPORATION, Roles.ASSOCIATION, Roles.INDIVIDUAL]
      },
      {
        route: "/editProfileInfo",
        execute: "editProfileInfo",
        method: HTTP_METHODS.PUT,
        schema: editProfileInfoSchema,
        roles: [Roles.CORPORATION, Roles.ASSOCIATION, Roles.INDIVIDUAL]
      }
    ];
  }

  async registerUser(req: Request, res: Response) {
    let userJson = req.body;
    userJson.password = await Password.hash(req.body.password);
    let user = new User(
      userJson.name,
      userJson.email,
      userJson.password,
      userJson.phone,
      userJson.address,
      userJson.roles,
      userJson.id,
      [userJson.addressCoord._latitude, userJson.addressCoord._longitude],
      userJson.picture
    );
    const result = await UserRepository.registerUser(user);
    res.status(result.statusCode).send(result);
  }

  async getAllUsers(req: Request, res: Response) {
    const result = await UserRepository.getAllUsers();
    res.status(result.statusCode).send(result);
  }

  async getProfileInfoById(req: Request, res: Response) {
    const userId = parseInt(req.params.id);
    const result = await UserRepository.getProfileInfoById(userId);
    res.status(result.statusCode).send(result);
  }

  async getProfileInfoByEmail(req: Request, res: Response) {
    const result = await UserRepository.getProfileInfoByEmail(req.params.email);
    res.status(result.statusCode).send(result);
  }

  async editProfileInfo(req: Request & any, res: Response) {
    const email = req.decoded.email;

    const currentUserRequest = await UserRepository.getProfileInfoByEmail(
      email
    );
    const userJson = currentUserRequest.data;

    const updatedUser: User = new User(
      userJson.name !== req.body.name ? req.body.name : userJson.name,
      userJson.email !== req.body.email ? req.body.email : userJson.email,
      req.body.password !== ""
        ? await Password.validate(req.body.password, userJson.password) ? userJson.password : await Password.hash(req.body.password)
        : userJson.password,
      userJson.phone !== req.body.phone ? req.body.phone : userJson.phone,
      userJson.address !== req.body.address ? req.body.address : userJson.address,
      Roles.INDIVIDUAL,
      userJson.id,
      [0, 0],
      userJson.picture !== req.body.picture ? req.body.picture : userJson.picture,
    );
    const result = await UserRepository.editProfileInfo(updatedUser);
    res.status(result.statusCode).send(result);
  }
}
