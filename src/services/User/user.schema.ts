import { celebrate, Joi } from 'celebrate';

  const editProfileInfoSchema = celebrate({
  query: Joi.object().keys({
    id: Joi.number(),
  })
}); 

const getProfileInfoByIdSchema = celebrate({
    query: Joi.object().keys({
      id: Joi.number(),
    })
  }); 

  const getProfileInfoByEmailSchema = celebrate({
    query: Joi.object().keys({
      email: Joi.string().email(),
    })
  }); 

  const   getAllUsersSchema = celebrate({
    query: Joi.object().keys({
      email: Joi.string().email(),
    })
  }); 

  const registerUserSchema = celebrate({
    query: Joi.object().keys({
        name: Joi.string(),
        email: Joi.string().email(),
        password : Joi.string(),
        address : Joi.string(),
        addressCoord : Joi.string(),
        phone: Joi.string().regex(/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/),
        picture : Joi.string(),
        roles: Joi.string()
    })
  }); 

export {
    getAllUsersSchema,
    registerUserSchema,
    editProfileInfoSchema,
    getProfileInfoByEmailSchema,
    getProfileInfoByIdSchema
  };