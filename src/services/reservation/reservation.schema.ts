const { celebrate, Joi } = require('celebrate');

  const getReservationSchema = celebrate({
  query: Joi.object().keys({
    id: Joi.number(),
  })
}); 
const getReservationByStatusSchema = celebrate({
  query: Joi.object().keys({
    status: Joi.string(),
  })
}); 
const reserveOfferSchema = celebrate({
  body: Joi.object().keys({
    id : Joi.number()
  })
});

const cancelReservationSchema = celebrate({
    body: Joi.object().keys({
        id : Joi.number()
    })
  });

export {
    getReservationSchema,
    getReservationByStatusSchema,
    reserveOfferSchema,
    cancelReservationSchema
};