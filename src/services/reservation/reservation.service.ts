import Service from "../../utils/service";
import HTTP_METHODS from "../../../ts/types/httpMethod.types";
import Roles from "../../../ts/types/roles.types";
import {
  getReservationSchema,
  reserveOfferSchema,
  cancelReservationSchema,
  getReservationByStatusSchema
} from "./reservation.schema";
import ReservationRepository from "../../repositories/reservation.repository";
import User from "../../../ts/interfaces/user.interfaces";
import Offer from "../../../ts/interfaces/offer.interfaces";
import Reservation from "../../../ts/interfaces/reservation.interfaces";
import OfferRepository from "../../repositories/offer.repository";
import Status from "../../../ts/types/status.types";
import ReservationStatus from "../../../ts/types/reservationStatus.types";
import { Request, Response } from "express";
import UserRepository from "../../repositories/user.repository";

export default class ReservationService extends Service {
  initRoute() {
    this.routesConfig = [
      {
        route: "/reservations",
        execute: "getReservations",
        method: HTTP_METHODS.GET,
        schema: getReservationSchema,
        roles: [
          Roles.INDIVIDUAL,
          Roles.ASSOCIATION,
          Roles.CORPORATION,
          Roles.ADMIN
        ]
      },
      {
        route: "/getReservation/:id",
        execute: "getReservationById",
        method: HTTP_METHODS.PUT,
        schema: cancelReservationSchema,
        roles: [
          Roles.INDIVIDUAL,
          Roles.ASSOCIATION,
          Roles.CORPORATION,
          Roles.ADMIN
        ]
      },
      {
        route: "/getReservation/:status",
        execute: "getReservationByStatus",
        method: HTTP_METHODS.GET,
        schema: getReservationByStatusSchema,
        roles: [
          Roles.INDIVIDUAL,
          Roles.ASSOCIATION,
          Roles.CORPORATION,
          Roles.ADMIN
        ]
      },
      {
        route: "/reserveOffer",
        execute: "reserveOffer",
        method: HTTP_METHODS.POST,
        schema: reserveOfferSchema,
        roles: [Roles.INDIVIDUAL, Roles.ASSOCIATION, Roles.ADMIN]
      },
      {
        route: "/cancelReservation",
        execute: "cancelReservation",
        method: HTTP_METHODS.PUT,
        schema: cancelReservationSchema,
        roles: [Roles.INDIVIDUAL, Roles.ASSOCIATION, Roles.ADMIN]
      }
    ];
  }

  async getReservationById(req: Request, res: Response) {
    const reservationId = parseInt(req.params.id);
    const result = await ReservationRepository.findReservationById(
      reservationId
    );
    res.status(result.statusCode).send(result);
  }

  async getReservationsByStatus(req: Request, res: Response) {
    const result = await ReservationRepository.findAllReservationByStatus(
      ReservationStatus[req.params.status]
    );
    res.status(result.statusCode).send(result);
  }

  async getReservations(req: Request & any, res: Response) {
    let user: User = req.decoded;
    const result = await ReservationRepository.findAllReservation(user.id);
    res.status(result.statusCode).send(result);
  }

  async reserveOffer(req: Request & any, res: Response) {
    let user: User = req.decoded;
    let offerId = req.body.id;
    let foundOffer = await OfferRepository.findOfferById(offerId);
    if (foundOffer.statusCode !== 200) {
      return res.status(foundOffer.statusCode).send(foundOffer);
    }
    foundOffer.data.status = Status.Reserved;
    foundOffer.data.author = new User("","","","","",Roles.ASSOCIATION,foundOffer.data.author);

    let updatedOffer = await OfferRepository.updateOffer(foundOffer.data);

    // temporary key generator
    const reservation = new Reservation(
      user,
      { ...foundOffer.data },
      new Date()
    );

    const result = await ReservationRepository.reserveOffer(reservation);
    res.status(result.statusCode).send(result);
  }

  async cancelReservation(req: Request, res: Response) {
    let reservationId = req.body.id;
    let foundReservation = await ReservationRepository.findReservationById(
      reservationId
    );

    if (foundReservation.statusCode !== 200) {
      return res.status(foundReservation.statusCode).send(foundReservation);
    }

    foundReservation.data.status = Status.Canceled;

    let updatedReservation = await ReservationRepository.cancelReservation(
      foundReservation.data
    );

    let offerResponse = await OfferRepository.findOfferById(
      foundReservation.data.offerId
    );
    let offer = Object.assign(new Offer(), offerResponse.data);
    offer.setStatus(Status.Opened);
    // console.log(offer);
    offer.author = new User("","","","","",Roles.ASSOCIATION,offer.author);
    await OfferRepository.updateOffer(offer);

    res.status(updatedReservation.statusCode).send(updatedReservation);
  }
}
