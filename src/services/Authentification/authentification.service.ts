import Service from "../../utils/service";
import HTTP_METHODS from "../../../ts/types/httpMethod.types";
import Roles from "../../../ts/types/roles.types";
import {
  postLoginSchema,
  resetPasswordSchema,
  changePwdSchema
} from "./authentification.schema";
import { Request, Response } from "express";
import Auth from "../../utils/auth";
import User from "../../../ts/interfaces/user.interfaces";
import JWT_CONFIG from "../../config/jwt.config";
import Notification from "../../../src/utils/notification";
import TypeMessage from "../../../ts/types/typeMessage.types";
import Password from "../../utils/password";
import UserRepository from "../../repositories/user.repository";
import ResponseRequest from "../../../ts/interfaces/ResponseRequest";

const jwt = require("jsonwebtoken");

var passport = require("passport");

export default class AuthentificationService extends Service {
  initRoute() {
    this.routesConfig = [
      {
        route: "/login",
        execute: "login",
        method: HTTP_METHODS.POST,
        schema: postLoginSchema,
        roles: [Roles.PUBLIC]
      },
      {
        route: "/getPasswordResetUrl",
        execute: "getPasswordResetUrl",
        method: HTTP_METHODS.POST,
        schema: resetPasswordSchema,
        roles: [Roles.PUBLIC]
      },
      {
        route: "/changePwd/:jwt",
        execute: "changePwd",
        method: HTTP_METHODS.POST,
        schema: changePwdSchema,
        roles: [Roles.PUBLIC]
      }
    ];
  }
  async changePwd(req: Request, res: Response) {
    const token = req.params.jwt;
    let email = "";
    try {
      email = jwt.verify(token, JWT_CONFIG.RESET_SECRET, {
        ignoreExpiration: true
      }).email;
    } catch (error) {
      const errResult = new ResponseRequest("failed")
        .setStatusCode(403)
        .setMessage("Invalid User")
        .setData(error);
      res.status(errResult.statusCode).send(errResult);
    }
    const pwd = await Password.hash(req.body.password);
    const currentUserRequest = await UserRepository.getProfileInfoByEmail(
      email
    );
    const userJson = currentUserRequest.data;
    const newUser = new User(
      userJson.name,
      userJson.email,
      userJson.password,
      userJson.phone,
      userJson.address,
      userJson.roles,
      userJson.id,
      [userJson.addressCoord._latitude, userJson.addressCoord._longitude],
      userJson.picture
    );
    newUser.password = pwd;
    const result = await UserRepository.editProfileInfo(newUser);
    res.status(result.statusCode).send(result);
  }
  async getPasswordResetUrl(req: Request, res: Response) {
    const userEmail = req.body.email;
    const userJWT = jwt.sign({ email: userEmail }, JWT_CONFIG.RESET_SECRET, {
      expiresIn: JWT_CONFIG.EXPIRES_IN
    });
    const sender = new User("TFOOD");
    const dest = new User(userEmail, userEmail);
    const link =
      "http://tfood.boostercamp-frontend.soat-sandbox.aws.soat.fr/resetPwd/" +
      userJWT;
    const message =
      "Bonjour, pour réinitialiser votre mot de passe, cliquez sur le lien suivant: " +
      link;
    const notification = new Notification(
      TypeMessage.MAIL,
      sender,
      dest,
      message
    );
    const notifResultawait = await notification.send();
    notifResultawait.setData({ jwt: userJWT });
    res.status(notifResultawait.statusCode).send(notifResultawait);
  }
  login(req: Request, res: Response) {
    return passport.authenticate(
      "local",
      { session: false },
      (err, passportUser: User, info) => {
        if (err) {
          return res.status(500).send(err.message);
        }

        if (passportUser) {
          return res.send({
            userName: passportUser.name,
            picture: passportUser.picture,
            roles: passportUser.roles,
            id: passportUser.id,
            address: passportUser.address,
            addressCoord: passportUser.addressCoord,
            jwt: Auth.sign({
              ...passportUser
            })
          });
        }

        return res.status(400).send(info);
      }
    )(req, res);
  }
}
