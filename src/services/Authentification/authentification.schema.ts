const { celebrate, Joi } = require("celebrate");

const postLoginSchema = celebrate({
  body: Joi.object().keys({
    email: Joi.string().email(),
    password: Joi.string()
  })
});
const resetPasswordSchema = celebrate({
  body: Joi.object().keys({
    email: Joi.string().email()
  })
});
const changePwdSchema = celebrate({
  body: Joi.object().keys({
    password: Joi.string()
  })
});

export { postLoginSchema, resetPasswordSchema, changePwdSchema };
