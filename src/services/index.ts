import OfferService from "./Offer/offer.service";
import ReservationService from "./reservation/reservation.service";
import UserService from "./User/user.service";
import AuthentificationService from "./Authentification/authentification.service";
import FileService from "./File/file.service";

export default [
  OfferService,
  ReservationService,
  UserService,
  AuthentificationService,
  FileService
];
