const { celebrate, Joi } = require("celebrate");

const postFileSchema = celebrate({
  query: Joi.object().keys({
    id: Joi.number(),
    description: Joi.string()
  })
});

export { postFileSchema };
