import Service from "../../utils/service";
import HTTP_METHODS from "../../../ts/types/httpMethod.types";
import Roles from "../../../ts/types/roles.types";
import { postFileSchema } from "./file.schema";
import OfferRepository from "../../repositories/offer.repository";
import OfferTarget from "../../../ts/types/offerTarget.types";
import { Response, Request } from "express";
import Offer from "../../../ts/interfaces/offer.interfaces";
import { IncomingForm } from "formidable";
import { copyFile } from "fs";
import { promisify } from "util";
import ResponseRequest from "../../../ts/interfaces/ResponseRequest";
import { join as PathJoin } from "path";
const randomString = require("randomstring");
const appRoot = require("app-root-path");

const CopyFile = promisify(copyFile);

export default class FileService extends Service {
  initRoute() {
    this.routesConfig = [
      {
        route: "/upload",
        execute: "upload",
        method: HTTP_METHODS.POST,
        schema: postFileSchema,
        roles: [
          Roles.PUBLIC
        ]
      }
    ];
  }

  async upload(req: Request, res: Response) {
    let form = new IncomingForm();
    let newPath = "";
    let newName = "";
    let response: ResponseRequest;
    form.on("file", async (field, file) => {
      let oldpath = file.path;
      let randomKey = randomString.generate(50);
      newName = randomKey + file.name;
      newPath = PathJoin(appRoot.path, "public", newName);
      try {
        let errCopy = await CopyFile(oldpath, newPath);
      } catch (e) {
        console.log(e);
      }
    });
    form.on("end", () => {
      response = new ResponseRequest("success")
        .setStatusCode(200)
        .setMessage("file uploaded")
        .setData({
          filePath: newName
        });
      res.json(response);
    });
    form.on("error", (error) => {
      response = new ResponseRequest("failed")
        .setStatusCode(500)
        .setData(error)
        .setMessage(error.message);
      res.json(response);
    });
    form.parse(req);
  }
}
