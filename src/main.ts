if (process.env.NODE_ENV === "development") require("dotenv").config();
import * as express from "express";

import Services from "./services";
import Service from "./utils/service";
var bodyParser = require("body-parser");
const { errors } = require("celebrate");
const swaggerUi = require("swagger-ui-express");
const swaggerDocument = require("./swagger.json");
var passport = require("passport");
var cors = require("cors");

export class Server {
  public app: express.Express;
  public port: number;
  public routes: Array<any>;

  constructor() {
    this.app = express();
    this.app.use(cors());
    this.app.use(express.static("public"));
    this.app.options("*", cors());
    this.app.use(bodyParser.json({limit: "50mb"}));
    this.app.use(bodyParser.urlencoded({limit: "50mb", extended: true, parameterLimit:50000}));
    
    this.app.use(
      "/api-docs",
      swaggerUi.serve,
      swaggerUi.setup(swaggerDocument)
    );
    this.app.use(passport.initialize());
    this.port = this.getPort();
    this.routes = [];
    this.setServices();
    this.setError();
    this.makeCelebrate();
    this.start();
  }

  private start = (): void => {
    if (!module.parent) {
      this.app.listen(this.port, this.onListen);
    }
  };

  private onListen = (err: any): void => {
    if (err) {
      console.error(err);
      return;
    }
    console.log(`> listening on port ${this.port}`);
  };

  private getPort = (): number => parseInt(process.env.PORT, 10) || 3000;

  private setServices = (): void => {
    for (let index = 0; index < Services.length; index++) {
      this.routes.push(new Services[index]());
      this.app.use("/", this.routes[index].getRouter());
    }
  };
  private makeCelebrate() {
    this.app.use(errors());
  }

  private setError() {
    this.app.use((err, req, res, next) => {
      if (res.headersSent) {
        return next(err);
      }
      console.error(err.stack);
      return res.status(err.status || 500).send({ message: err });
    });
  }
}

export default new Server().app;
