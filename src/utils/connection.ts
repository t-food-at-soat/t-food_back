let mysql = require('mysql-promise')();
let config = require('../config/db-config');

let connection = mysql.configure({
  host: config.MYSQL_HOST,
  user: config.MYSQL_USER,
  password: config.MYSQL_PWD,
  database: config.MYSQL_DB
});



export default mysql;