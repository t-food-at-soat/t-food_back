import { HTTP_ERROR_CODES, SQL_ERROR_CODES } from "../../ts/types/errorCode.types";
import ERRORS from "../../ts/types/error.types";

class HttpResolver {

  static unauthorized(res) {
    res.status(401).send({
      code: HTTP_ERROR_CODES.UNAUTHORIZED,
      message: ERRORS.UNAUTHORIZED
    });
  }

  static tokenExpired(response) {
    return response.status(401).send({
      code: HTTP_ERROR_CODES.TOKEN_EXPIRED,
      message: ERRORS.TOKEN_EXPIRED
    });
  }
}
export default HttpResolver;