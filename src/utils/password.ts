const bcrypt = require('bcryptjs');

class Password {

  static async hash(password) {
    const saltRounds = 10;
    const hashedPassword = await bcrypt.hash(password, saltRounds);

    return hashedPassword;
  }

  static async validate(userSendPassword : string, userBddPassword : string) {
    return await bcrypt.compare(userSendPassword, userBddPassword);
  }

}

export default Password;