import Roles from "../../ts/types/roles.types";
const jwt = require("jsonwebtoken");
var passport = require("passport");
var LocalStrategy = require("passport-local").Strategy;
import JWT_CONFIG from "../config/jwt.config";
import User from "../../ts/interfaces/user.interfaces";
import Password from "./password";
import UserService from "../services/User/user.service";
import UserRepository from "../repositories/user.repository";

class Auth {
  static sign(data) {
    return jwt.sign(data, JWT_CONFIG.SECRET, {
      expiresIn: JWT_CONFIG.EXPIRES_IN
    });
  }

  static verify(token) {
    return jwt.verify(token, JWT_CONFIG.SECRET, { ignoreExpiration: true });
  }

  static checkMultiRoles(authorizedRoles, userRoles) {
    return authorizedRoles.includes(userRoles);
  }

  static hasPublicRole(authorizedRoles) {
    return authorizedRoles.find(role => {
      return role === Roles.PUBLIC;
    });
  }
}

passport.use(
  new LocalStrategy(
    {
      usernameField: "email",
      passwordField: "password"
    },
    async function(email, password, done) {
      const res = await UserRepository.getProfileInfoByEmail(email);
      const user = res.data;
      if (!user) return done(null, false, { message: "Incorrect username." });

      if (!(await Password.validate(password, user.password))) {
        return done(null, false, { message: "Incorrect password." });
      }
      return done(null, user);
    }
  )
);

export default Auth;
