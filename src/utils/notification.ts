import User from "../../ts/interfaces/user.interfaces";
import ResponseRequest from "../../ts/interfaces/ResponseRequest";
import GmailConf from "../config/gmail.config";
import nexmoConf from "../config/nexmo.config";
import TypeMessage from "../../ts/types/typeMessage.types";
const Nexmo = require("nexmo");

const send = require("gmail-send")({
  user: GmailConf.mail,
  pass: GmailConf.pass
});

const nexmo = new Nexmo({
  apiKey: nexmoConf.apiKey,
  apiSecret: nexmoConf.apiSecret
});

export default class Notification {
  public id: number;
  public type: TypeMessage;
  public message: string;
  public sender: User;
  public receiver: User;
  public datetime: Date;

  constructor(
    type: TypeMessage,
    sender: User,
    receiver: User,
    message: string,
    id?: number
  ) {
    this.type = type;
    this.sender = sender;
    this.receiver = receiver;
    this.message = message;
    this.id = id;
    this.datetime = new Date(Date.now());
  }

  async send(): Promise<ResponseRequest> {
    try {
      if (this.type === TypeMessage.MAIL) {
        const res = await send({
          to: this.receiver.email,
          subject: "TFOOD - Notification",
          text: this.message
        });

        return new ResponseRequest("success").setMessage("Mail send");
      } else {
        const res = nexmo.message.sendSms(
          this.sender.phone,
          this.receiver.phone,
          this.message
        );

        return new ResponseRequest("success").setMessage("Mail send");
      }
    } catch (e) {
      return new ResponseRequest("failed")
        .setMessage(e.message)
        .setStatusCode(500);
    }
  }
}
