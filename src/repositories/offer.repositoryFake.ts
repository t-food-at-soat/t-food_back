import Offer from "../../ts/interfaces/offer.interfaces";
import OfferTarget from "../../ts/types/offerTarget.types";
import ResponseRequest from "../../ts/interfaces/ResponseRequest";

let testData = {
    "Offers" : [
        
    ],
    "Users" : [
    ],
    "Reservations" : [
    ],
    "Notifications" : [
    ]
};

export default class OfferRepository {
    static async addOffer(newOffer : Offer) : Promise<ResponseRequest>{
        try {
            const req = await testData.Offers.push(newOffer);
            return new ResponseRequest("success");
                
        } catch (e) {
            return new ResponseRequest("failed")
                .setStatusCode(500)
                .setMessage(e.message);
        }
    }

    static async findAllOrdersByTargets(offerTarget : OfferTarget) : Promise<ResponseRequest>{
        try {
            const req = await testData.Offers.filter((item) => item.target === offerTarget);
            return new ResponseRequest("success")
            .setData(req);
        } catch (e) {
            return new ResponseRequest("failed")
                .setStatusCode(500)
                .setMessage(e.message);
        }
    }

    static async findAllOrdersByAuthor(authorName : string){
        try {
            const req = await testData.Offers.filter((item) => item.author === authorName);
            return {status : "success", data : req};
        } catch (e) {
            return {status : "failed", message : e.message};
        }
    }
    static async findAllOrdersSortedByNewest(offerTarget : OfferTarget){
        try {
            let req = await testData.Offers.filter((item) => item.target === offerTarget);
            req.sort(function(itemA, itemB){
                return  itemA.datetime.getTime() - itemB.datetime.getTime();
            })
            return new ResponseRequest("success")
            .setData(req);
        } catch (e) {
            return new ResponseRequest("failed")
            .setStatusCode(500)
            .setMessage(e.message);
        }
    }
    static async findAllOrdersSortedByOldest(offerTarget : OfferTarget) : Promise<ResponseRequest>{
        try {
            let req = await testData.Offers.filter((item) => item.target === offerTarget);
            req.sort(function(itemA, itemB){
                return  itemB.datetime.getTime() - itemA.datetime.getTime();
            })
            return new ResponseRequest("success")
            .setData(req);
        } catch (e) {
            return new ResponseRequest("failed")
            .setStatusCode(500)
            .setMessage(e.message);
        }
    }
    static async findOfferById(offerId : number) : Promise<ResponseRequest> {
        try {
            let req = await testData.Offers.filter((item) => {
                return item.id === offerId
            });
            if(req.length === 0)
                return new ResponseRequest("failed")
                    .setStatusCode(404)
                    .setMessage("data not found");
            return new ResponseRequest("success")
                .setData(req[0]);
        } catch (e) {
            return new ResponseRequest("failed")
                .setStatusCode(500)
                .setMessage(e.message);
        }
    }

    static async updateOffer(updatedOffer : Offer) : Promise<ResponseRequest>{
        try {
            let indexFind = await testData.Offers.findIndex((item) => {
                return item.id === updatedOffer.id;
            });
            if(indexFind === -1)
                return new ResponseRequest("failed")
                    .setStatusCode(500)
                    .setMessage("undefined Offer");
            
            testData.Offers[indexFind] = updatedOffer;
            return new ResponseRequest("success")
                .setData(testData.Offers[indexFind]);
                      
        } catch (e) {
            return new ResponseRequest("failed")
            .setStatusCode(500)
            .setMessage(e.message);
        }
    }

    static async deleteOffer(id: number) : Promise<ResponseRequest> {
        try {
            let indexFind = await testData.Offers.findIndex((item) => {
                return item.id === id;
            });
            if(indexFind === -1)
                return new ResponseRequest("failed")
                    .setStatusCode(500)
                    .setMessage("undefined Offer");
            
            testData.Offers.splice(indexFind, 1);
            return new ResponseRequest("success")
                .setData(testData.Offers);
        
        } catch (e) {
            return new ResponseRequest("failed")
                .setStatusCode(500)
                .setMessage(e.message);
        }
    }

    static async breakRepo(){
        testData = null;
    }

    static async createRepo(){
        testData = {
            "Offers" : [
                
            ],
            "Users" : [
            ],
            "Reservations" : [
            ],
            "Notifications" : [
            ]
        };
    }



};