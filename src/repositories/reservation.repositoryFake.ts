import Offer from "../../ts/interfaces/offer.interfaces";
import User from "../../ts/interfaces/user.interfaces";
import Reservation from "../../ts/interfaces/reservation.interfaces";
import ReservationStatus from "../../ts/types/reservationStatus.types";
import ResponseRequest from "../../ts/interfaces/ResponseRequest";

let testData = {
    "Offers" : [
        
    ],
    "Users" : [
    ],
    "Reservations" : [
    ],
    "Notifications" : [
    ]
};
export default class ReservationRepository {

    static async findAllReservation(){
        try {
            const req = testData.Reservations;
            return new ResponseRequest("success")
            .setData(req);
        } catch (e) {
            return new ResponseRequest("failed")
            .setStatusCode(500)
            .setMessage(e.message);
        }
    }

    static async findReservationById(reservationId : number) {
        try {
            let req =  testData.Reservations.filter((item) => {
                return item.id === reservationId
            });
            if(req.length === 0){
                return new ResponseRequest("failed")
                .setStatusCode(404)
                .setMessage("Reservation not found");
            }
            return new ResponseRequest("success")
            .setData(req[0]);
        } catch (e) {
            return new ResponseRequest("failed")
            .setStatusCode(500)
            .setMessage(e.message);
        }
    }

    static async findAllReservationByStatus(status : ReservationStatus){
        try {
            const req = testData.Reservations.filter((reservation) => reservation.status === status);;
            return new ResponseRequest("success")
            .setData(req);
        } catch (e) {
            return new ResponseRequest("failed")
            .setStatusCode(500)
            .setMessage(e.message);
        }
    }
    
    static async reserveOffer(reservation : Reservation){
        try {
            const req = testData.Reservations.push(reservation);
            return new ResponseRequest("success")
            .setMessage("Offer reserved");
        } catch (e) {
            return new ResponseRequest("failed")
            .setStatusCode(500)
            .setMessage(e.message);
        }
    }

    static async cancelReservation(reservation : Reservation){
        try {
            let indexFind = testData.Reservations.findIndex((item) => {
                return item.id === reservation.id;
            });
            if(indexFind === -1)
                return new ResponseRequest("failed")
                .setStatusCode(500)
                .setMessage("undefined Offer");

            reservation.setStatus(ReservationStatus.Canceled);
            testData.Reservations[indexFind] = reservation;
            return new ResponseRequest("success")
            .setData(testData.Reservations[indexFind]);
                      
        } catch (e) {
            return new ResponseRequest("failed")
            .setStatusCode(500)
            .setMessage(e.message);
        }
    }

    static async breakRepo(){
        testData = null;
    }

    static async createRepo(){
        testData = {
            "Offers" : [
                
            ],
            "Reservations" : [

            ],
            "Users" : [
            ],
            "Notifications" : [
            ]
        };
    }
}