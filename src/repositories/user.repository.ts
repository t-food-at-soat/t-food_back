import Offer from "../../ts/interfaces/offer.interfaces";
import OfferTarget from "../../ts/types/offerTarget.types";
import ResponseRequest from "../../ts/interfaces/ResponseRequest";
import User from "../../ts/interfaces/user.interfaces";
import con from "../utils/connection";

export default class UserRepository {
  static async getAllUsers() {
    try {
      var sql = "select * from users;";
      let [rows, fields] = await con.query(sql);
      return new ResponseRequest("success").setData(rows);
    } catch (e) {
      return new ResponseRequest("failed")
        .setStatusCode(500)
        .setMessage(e.message);
    }
  }

  static async registerUser(newUser: User) {
    try {
      var sql =
        "insert into users " +
        " (`email`, `phone`, `address`, `addressCoord`," +
        "`picture`, `password`, `name`, `roles`) " +
        "VALUES (?, ?, ?, ?, ?, ?, ?, ?);";
      let [rows, fields] = await con.query(sql, [
        newUser.email,
        newUser.phone,
        newUser.address,
        JSON.stringify(newUser.addressCoord),
        newUser.picture,
        newUser.password,
        newUser.name,
        newUser.roles
      ]);
      return new ResponseRequest("success")
        .setInsertedId(rows.insertId)
        .setMessage("Utilisateur ajoutée");
    } catch (e) {
      return new ResponseRequest("failed")
        .setStatusCode(500)
        .setMessage(e.message);
    }
  }

  static async getProfileInfoByEmail(email: string) {
    try {
      var sql = "select * from users where email = ?;";
      let [rows, fields] = await con.query(sql, [email]);
      if (rows.length === 0)
        return new ResponseRequest("failed")
          .setStatusCode(404)
          .setMessage("undefined user");
      return new ResponseRequest("success").setData(rows[0]);
    } catch (e) {
      return new ResponseRequest("failed")
        .setStatusCode(500)
        .setMessage(e.message);
    }
  }

  static async editProfileInfo(updatedUser: User): Promise<ResponseRequest> {
    try {
      var sql =
        "update users set " +
        "email=?," +
        "phone=?," +
        "address=?," +
        "  addressCoord=?," +
        " picture= ?," +
        " password=?," +
        " name=? " +
        "where id=?";
      let [rows, fields] = await con.query(sql, [
        updatedUser.email,
        updatedUser.phone,
        updatedUser.address,
        updatedUser.addressCoord.toString(),
        updatedUser.picture,
        updatedUser.password,
        updatedUser.name,
        updatedUser.id
      ]);

      return new ResponseRequest("success")
        .setInsertedId(rows.insertId)
        .setMessage("User updated");
    } catch (e) {
      return new ResponseRequest("failed")
        .setStatusCode(500)
        .setMessage(e.message)
        .setSqlMessage(e.sql);
    }
  }

  static async getProfileInfoById(userId: number): Promise<ResponseRequest> {
    try {
      var sql = "select * from users where id = ?;";
      let [rows, fields] = await con.query(sql, [userId]);
      if (rows.length === 0)
        return new ResponseRequest("failed")
          .setStatusCode(404)
          .setMessage("undefined user");
      return new ResponseRequest("success").setData(rows[0]);
    } catch (e) {
      return new ResponseRequest("failed")
        .setStatusCode(500)
        .setMessage(e.message);
    }
  }
}
