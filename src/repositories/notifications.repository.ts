import Offer from "../../ts/interfaces/offer.interfaces";
import OfferTarget from "../../ts/types/offerTarget.types";
import ResponseRequest from "../../ts/interfaces/ResponseRequest";
import con from "../utils/connection";
import Notification from "../utils/notification"
import User from "../../ts/interfaces/user.interfaces";

export default class NotificationRepository {
    static async addNotification(notif : Notification) : Promise<ResponseRequest>{
        try {
            var sql = "insert into notifications " 
                       +" (`datetime`, `target`, `type`, `message`,"
                       + "`author`) "
                       + "VALUES (?, ?, ?, ?, ?);"
            let [rows, fields] = await con.query(sql, [notif.datetime, notif.receiver.id,
                notif.type, notif.message, notif.sender.id]);
            return new ResponseRequest("success")
            .setInsertedId(rows.insertId)
            .setMessage("Notification ajoutée");
                
        } catch (e) {
            return new ResponseRequest("failed")
                .setStatusCode(500)
                .setMessage(e.message)
                .setSqlMessage(e.sql);
        }
    }

    static async getAllUserReveivedNotifications(user : User) : Promise<ResponseRequest>{
        try {
            var sql = "select * from notification where target = ? ;"
            let [rows, fields] = await con.query(sql, [user.id]);
            return new ResponseRequest("success")
            .setData(rows);
        } catch (e) {
            return new ResponseRequest("failed")
                .setStatusCode(500)
                .setMessage(e.message);
        }
    }

    static async getAllUserSentNotifications(user : User) : Promise<ResponseRequest>{
        try {
            var sql = "select * from notification where author = ? ;"
            let [rows, fields] = await con.query(sql, [user.id]);
            return new ResponseRequest("success")
            .setData(rows);
        } catch (e) {
            return new ResponseRequest("failed")
                .setStatusCode(500)
                .setMessage(e.message);
        }
    }

    
    
    
    static async findNotificationById(notifId : number) : Promise<ResponseRequest> {
        try {
            var sql = "select * from notifications where id = ?;"
            let [rows, fields] = await con.query(sql, [notifId]);
            
            if(rows.length === 0)
                return new ResponseRequest("failed")
                    .setStatusCode(404)
                    .setMessage("data not found");

            return new ResponseRequest("success")
            .setData(rows[0]);
        } catch (e) {
            return new ResponseRequest("failed")
                .setStatusCode(500)
                .setMessage(e.message);
        }
    }

    
};