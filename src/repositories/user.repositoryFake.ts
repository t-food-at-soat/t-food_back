import Offer from "../../ts/interfaces/offer.interfaces";
import OfferTarget from "../../ts/types/offerTarget.types";
import ResponseRequest from "../../ts/interfaces/ResponseRequest";
import User from "../../ts/interfaces/user.interfaces";

let testData = {
    "Offers" : [
        
    ],
    "Users" : [
    ],
    "Reservations" : [
    ],
    "Notifications" : [
    ]
};

export default class UserRepository {


    static async getAllUsers() {
        try {
            const req =  testData.Users;
            return new ResponseRequest("success")   
            .setData(req)
        } catch (e) {
            return new ResponseRequest("failed")
                .setStatusCode(500)
                .setMessage(e.message);
    }
}

    static async registerUser(newUser : User){
        try {
            const req = testData.Users.push(newUser);
            return new ResponseRequest("success")   
        } catch (e) {
            return new ResponseRequest("failed")
                .setStatusCode(500)
                .setMessage(e.message);
        }
    }

    static async getProfileInfoByEmail(email: any) {
        try {
            const req = testData.Users.filter((user) => user.email === email);
            if(req.length === 0)
                return new ResponseRequest("failed")
                    .setStatusCode(404)
                    .setMessage("data not found");
            return new ResponseRequest("success")
                .setData(req[0]);
        } catch (e) {
            return new ResponseRequest("failed")
                .setStatusCode(500)
                .setMessage(e.message);
        }
    }

    static async editProfileInfo(updatedUser : User) : Promise<ResponseRequest>{
        try {
            let indexFind = testData.Users.findIndex((user) => {
                return user.email === updatedUser.email;
            });
            if(indexFind === -1)
                return new ResponseRequest("failed")
                    .setStatusCode(404)
                    .setMessage("undefined user");
            testData.Users[indexFind] = updatedUser;
            return new ResponseRequest("success")
                .setData(testData.Users[indexFind]);
                      
        } catch (e) {
            return new ResponseRequest("failed")
            .setStatusCode(500)
            .setMessage(e.message);
        }
    }

    static async getProfileInfoById(userId : number) : Promise<ResponseRequest>{
        try {
            //TODO: replace the mock by an sql request 
            // 'SELECT * FROM USER WHEN user.id = $userId$'
            const req = testData.Users.filter((user) => user.id === userId);
            return new ResponseRequest("success")
            .setData(req[0]);
        } catch (e) {
            return new ResponseRequest("failed")
                .setStatusCode(500)
                .setMessage(e.message);
        }
    }

    static async breakRepo(){
        testData = null;
    }

    static async createRepo(){
        testData = {
            "Offers" : [
                
            ],
            "Users" : [
                {
                    id: 4,
                    name: "Soat",
                    email: "exemple@test.com",
                    address: "address",
                    password: "********",
                    roles: "particulier"
                }
            ],
            "Reservations" : [
            ],
            "Notifications" : [
            ]
        };
    }
}