import Offer from "../../ts/interfaces/offer.interfaces";
import OfferTarget from "../../ts/types/offerTarget.types";
import ResponseRequest from "../../ts/interfaces/ResponseRequest";
import con from "../utils/connection";
import OfferType from "../../ts/types/offerType.types";

import Status from "../../ts/types/status.types";

export default class OfferRepository {
  static async addOffer(newOffer: Offer): Promise<ResponseRequest> {
    try {
      var sql =
        "insert into offers " +
        " (`description`, `datetime`, `type`, `target`," +
        "`quantity`, `location`, `picture`, `author`) " +
        "VALUES (?, ?, ?, ?, ?, ?, ?, ?);";
      let [rows, fields] = await con.query(sql, [
        newOffer.description,
        newOffer.datetime,
        newOffer.type,
        newOffer.target,
        newOffer.quantity,
        newOffer.location,
        newOffer.picture,
        newOffer.author.id
      ]);
      return new ResponseRequest("success")
        .setInsertedId(rows.insertId)
        .setMessage("Offre ajoutée");
    } catch (e) {
      return new ResponseRequest("failed")
        .setStatusCode(500)
        .setMessage(e.message)
        .setSqlMessage(e.sql);
    }
  }

  static async findAllOrdersByTargets(
    offerTarget: OfferTarget
  ): Promise<ResponseRequest> {
    try {
      const sql =
        "select " +
        "offers.id," +
        "offers.description, " +
        "offers.status, " +
        "offers.datetime,	" +
        "offers.type,	" +
        "offers.target,	" +
        "offers.quantity,	" +
        "offers.picture,	" +
        "offers.location,	" +
        "offers.author,	" +
        "users.id as userID,	" +
        "users.picture as userPicture,	" +
        "users.name as username " +
        "from	" +
        "offers,	" +
        "users " +
        "where	" +
        "users.id = offers.author	" +
        "and	target = ? 	" +
        "and status = ? " +
        "order by offers.datetime;";
      let [rows, fields] = await con.query(sql, [offerTarget, Status.Opened]);
      for (let i = 0; i < rows.length; ++i) {
        rows[i].author = {
          id: rows[i].userID,
          name: rows[i].username,
          picture: rows[i].userPicture
        };
        delete rows[i].userID;
        delete rows[i].username;
        delete rows[i].userPicture;
      }
      return new ResponseRequest("success").setData(rows);
    } catch (e) {
      return new ResponseRequest("failed")
        .setStatusCode(500)
        .setMessage(e.message)
        .setSqlMessage(e.sql);
    }
  }

  static async findAllOrdersByAuthor(authorName: string) {
    try {
      let sql = "select id, name, picture from users where name = ?";
      let [rows, fields] = await con.query(sql, [authorName]);

      sql = "select * from offers where author = " + rows[0].id;
      let [rowsOffer, fieldsOffer] = await con.query(sql);
      for (let i = 0; i < rowsOffer.length; ++i) {
        rowsOffer[i].author = rows[0];
      }
      return new ResponseRequest("success").setData(rowsOffer);
    } catch (e) {
      return new ResponseRequest("failed")
        .setStatusCode(500)
        .setMessage(e.message);
    }
  }
  static async findAllOrdersSortedByNewest(offerTarget: OfferTarget) {
    try {
      var sql =
        "select * from offers where target = ?  and status = ? order by datetime;";
      let [rows, fields] = await con.query(sql, [offerTarget, Status.Opened]);
      return new ResponseRequest("success").setData(rows);
    } catch (e) {
      return new ResponseRequest("failed")
        .setStatusCode(500)
        .setMessage(e.message);
    }
  }
  static async findAllOrdersSortedByOldest(
    offerTarget: OfferTarget
  ): Promise<ResponseRequest> {
    try {
      var sql =
        "select * from offers where target = ?  and status = ? order by datetime desc;";
      let [rows, fields] = await con.query(sql, [offerTarget, Status.Opened]);
      return new ResponseRequest("success").setData(rows);
    } catch (e) {
      return new ResponseRequest("failed")
        .setStatusCode(500)
        .setMessage(e.message);
    }
  }
  static async findOfferById(offerId: number): Promise<ResponseRequest> {
    
    try {
      var sql = "select * from offers where id = ?;";
      let [rows, fields] = await con.query(sql, [offerId]);
      return new ResponseRequest("success").setData(rows[0]);
    } catch (e) {
      return new ResponseRequest("failed")
        .setStatusCode(500)
        .setMessage(e.message);
    }
  }

  static async updateOffer(updatedOffer: Offer): Promise<ResponseRequest> {
    try {
      var sql =
        "update offers set description=?,datetime=?,status=?, type=?, target= ?, quantity=?, location=?, picture=?, author=? where id=?";
      let [rows, fields] = await con.query(sql, [
        updatedOffer.description,
        updatedOffer.datetime,
        updatedOffer.status,
        updatedOffer.type,
        updatedOffer.target,
        updatedOffer.quantity,
        updatedOffer.location,
        updatedOffer.picture,
        updatedOffer.author.id,
        updatedOffer.id
      ]);

      if (rows.affectedRows === 0)
        return new ResponseRequest("failed")
          .setStatusCode(404)
          .setMessage("undefined Offer");

      return new ResponseRequest("success").setInsertedId(rows.insertId);
    } catch (e) {
      console.log(e);
      return new ResponseRequest("failed")
        .setStatusCode(500)
        .setMessage(e.message);
    }
  }

  static async deleteOffer(id: number): Promise<ResponseRequest> {
    try {
      var sql = "delete from offers " + "where id=?";
      let [rows, fields] = await con.query(sql, [id]);

      if (rows.affectedRows === 0)
        return new ResponseRequest("failed")
          .setStatusCode(404)
          .setMessage("undefined Offer");

      return new ResponseRequest("success").setInsertedId(rows.insertId);
    } catch (e) {
      return new ResponseRequest("failed")
        .setStatusCode(500)
        .setMessage(e.message);
    }
  }
}
