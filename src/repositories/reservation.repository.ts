import Offer from "../../ts/interfaces/offer.interfaces";
import User from "../../ts/interfaces/user.interfaces";
import Reservation from "../../ts/interfaces/reservation.interfaces";
import ReservationStatus from "../../ts/types/reservationStatus.types";
import ResponseRequest from "../../ts/interfaces/ResponseRequest";
import con from "../utils/connection";

export default class ReservationRepository {
  static async findAllReservation(userId: number) {
    try {
      const sql =
        "select " +
        "offers.id," +
        "offers.description, " +
        "offers.status, " +
        "offers.datetime, " +
        "offers.type, " +
        "offers.target, " +
        "offers.quantity, " +
        "offers.picture, " +
        "offers.location, " +
        "offers.author, " +
        "users.id as userUserID, " +
        "users.picture as userPicture, " +
        "users.name as username, " +
        "reservations.id as ReservationId, " +
        "reservations.userID as ReservationUserId, " +
        "reservations.status as ReservationStatus " +
        "from " +
        "offers, " +
        "reservations, " +
        "users " +
        "where " +
        "reservations.userID = ? " +
        "and offers.id = reservations.offerId " +
        "and users.id = offers.author " +
        "order by offers.datetime;";

      let [rows, fields] = await con.query(sql, [userId]);

      for (let i = 0; i < rows.length; ++i) {
        rows[i].author = {
          id: rows[i].userUserID,
          name: rows[i].username,
          picture: rows[i].userPicture
        };
        delete rows[i].userUserID;
        delete rows[i].username;
        delete rows[i].userPicture;
      }
      return new ResponseRequest("success").setData(rows);
    } catch (e) {
      console.log(e);
      return new ResponseRequest("failed")
        .setStatusCode(500)
        .setMessage(e.message)
        .setSqlMessage(e.sql);
    }
  }

  static async findReservationById(reservationId: number) {
    try {
      var sql = "select * from reservations where id=?;";
      let [rows, fields] = await con.query(sql, [reservationId]);

      if (rows.length === 0) {
        return new ResponseRequest("failed")
          .setStatusCode(404)
          .setMessage("Reservation not found");
      }
      return new ResponseRequest("success").setData(rows[0]);
    } catch (e) {
      return new ResponseRequest("failed")
        .setStatusCode(500)
        .setMessage(e.message);
    }
  }

  static async findAllReservationByStatus(status: ReservationStatus) {
    try {
      var sql = "select * from reservations where status=?;";
      let [rows, fields] = await con.query(sql, [status]);
      return new ResponseRequest("success").setData(rows);
    } catch (e) {
      return new ResponseRequest("failed")
        .setStatusCode(500)
        .setMessage(e.message);
    }
  }

  static async reserveOffer(reservation: Reservation) {
    try {
      const checkForExistingReservation =
        "select * from reservations where offerId = ? and status = ?;";
      let [rowResa, fieldResa] = await con.query(checkForExistingReservation, [
        reservation.offer.id,
        ReservationStatus.Passed
      ]);

      if (rowResa.length > 0) {
        return new ResponseRequest("failed")
          .setStatusCode(403)
          .setMessage("This offer has been reserved by someone else");
      }
      var sql =
        "insert into reservations " +
        " (`userID`, `offerId`, `datetime`) " +
        "VALUES (?, ?, ?);";
      let [rows, fields] = await con.query(sql, [
        reservation.user.id,
        reservation.offer.id,
        reservation.datetime
      ]);
      return new ResponseRequest("success")
        .setInsertedId(rows.insertId)
        .setMessage("Offre ajoutée");
    } catch (e) {
      console.log(e);
      return new ResponseRequest("failed")
        .setStatusCode(500)
        .setMessage(e.message);
    }
  }

  static async cancelReservation(reservation: Reservation & any) {
    try {
      var sql =
        "update reservations set " +
        "status=? " +
        "where id=? and status=? and userId = ?;";
      let [rows, fields] = await con.query(sql, [
        ReservationStatus.Canceled,
        reservation.id,
        ReservationStatus.Passed,
        reservation.userID
      ]);
      if (rows.length === 0)
        return new ResponseRequest("failed")
          .setStatusCode(500)
          .setMessage("This offer doesn't existe or is not reserved");

      return new ResponseRequest("success").setInsertedId(rows.insertId);
    } catch (e) {
      return new ResponseRequest("failed")
        .setStatusCode(500)
        .setMessage(e.message);
    }
  }
}
