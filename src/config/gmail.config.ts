const GmailConf = {
    mail : process.env.GMAIL_MAIL,
    pass : process.env.GMAIL_PASS
};

export default GmailConf;