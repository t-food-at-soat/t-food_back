const JWT = {
  SECRET: "trex",
  RESET_SECRET: "trexpwd",
  EXPIRES_IN: 14400
};

export default JWT;
