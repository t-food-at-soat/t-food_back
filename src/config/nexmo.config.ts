const nexmoConf = {
    apiKey: process.env.NEXMO_APIKEY,
    apiSecret: process.env.NEXMO_APISECRET
};

export default nexmoConf;