SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

CREATE DATABASE IF NOT EXISTS `t_food` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `t_food`;

DROP TABLE IF EXISTS `notifications`;
CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datetime` datetime NOT NULL,
  `target` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `message` text NOT NULL,
  `author` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `target` (`target`),
  KEY `author` (`author`),
  KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `notificationstype`;
CREATE TABLE IF NOT EXISTS `notificationstype` (
  `type` varchar(32) NOT NULL,
  PRIMARY KEY (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `notificationstype` (`type`) VALUES
('mail'),
('notification'),
('sms');

DROP TABLE IF EXISTS `offers`;
CREATE TABLE IF NOT EXISTS `offers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(400) NOT NULL,
  `status` varchar(32) NOT NULL DEFAULT 'Opened',
  `datetime` datetime NOT NULL,
  `type` varchar(32) NOT NULL,
  `target` varchar(32) NOT NULL,
  `quantity` int(11) NOT NULL,
  `location` varchar(256) DEFAULT NULL,
  `picture` text NOT NULL,
  `author` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `status` (`status`),
  KEY `target` (`target`),
  KEY `type` (`type`),
  KEY `author` (`author`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

INSERT INTO `offers` (`id`, `description`, `status`, `datetime`, `type`, `target`, `quantity`, `location`, `picture`, `author`) VALUES
(1, 'meetup google+', 'Opened', '2019-10-25 15:10:22', 'Pizza', 'Associations', 5, '8 Rue de Londres, 75009 Paris, France', 'https://www.atelierdeschefs.com/media/recette-e30299-pizza-pepperoni-tomate-mozza.jpg', 2),
(2, 'Gala Android', 'Opened', '2019-10-26 15:12:39', 'Buffet froid', 'Associations', 3, '2 Rue Mozart, 92110 Clichy, France', 'https://www.flunch-traiteur.fr/cms/wp-content/uploads/2018/12/grand-buffet-froid-min.jpg', 2),
(3, 'So Food At Soat', 'Opened', '2019-10-25 16:14:04', 'Salades', 'Associations', 6, '89 Quai Panhard et Levassor, 75013 Paris, France', 'http://tfood-server.hd3midcur3.eu-west-1.elasticbeanstalk.com/NXEUbhEUpR2MRtnNMRIwZzbPqkZ2VW0WTWV2ZNGFNVWBhPc2LUEDu9etMW4AMmePk.jpeg', 1),
(4, 'facebook gala', 'Reserved', '2019-10-25 15:16:42', 'Buffet chaud', 'Associations', 12, '6 Rue Ménars, 75002 Paris, France', 'https://media-cdn.tripadvisor.com/media/photo-s/0d/6f/39/2d/buffet-chaud.jpg', 3),
(5, 'Boostercamp soirée ', 'Opened', '2019-10-24 21:00:50', 'Pizza', 'Associations', 15, '89 Quai Panhard et Levassor, 75013 Paris, France', 'http://48998aac.ngrok.io/F28nUGEHwcki354XmP5oxF76acnlvwLT0IVOWpngF8jlhDAEJaimage.jpg', 1);

DROP TABLE IF EXISTS `offerstatus`;
CREATE TABLE IF NOT EXISTS `offerstatus` (
  `status` varchar(32) NOT NULL,
  PRIMARY KEY (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `offerstatus` (`status`) VALUES
('Canceled'),
('Closed'),
('Opened'),
('Reserved');

DROP TABLE IF EXISTS `offertarget`;
CREATE TABLE IF NOT EXISTS `offertarget` (
  `target` varchar(32) NOT NULL,
  PRIMARY KEY (`target`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `offertarget` (`target`) VALUES
('Associations'),
('Particuliers');

DROP TABLE IF EXISTS `offertype`;
CREATE TABLE IF NOT EXISTS `offertype` (
  `type` varchar(32) NOT NULL,
  PRIMARY KEY (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `offertype` (`type`) VALUES
('Buffet chaud'),
('Buffet froid'),
('Pizza'),
('Salades');

DROP TABLE IF EXISTS `reservations`;
CREATE TABLE IF NOT EXISTS `reservations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `offerId` int(11) NOT NULL,
  `status` varchar(32) NOT NULL DEFAULT 'Passed',
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `offerId` (`offerId`),
  KEY `status` (`status`),
  KEY `userID` (`userID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

INSERT INTO `reservations` (`id`, `userID`, `offerId`, `status`, `datetime`) VALUES
(1, 5, 1, 'Canceled', '2019-10-24 17:20:20'),
(2, 6, 4, 'Passed', '2019-10-24 18:29:59'),
(3, 6, 5, 'Canceled', '2019-10-24 18:33:22');

DROP TABLE IF EXISTS `reservationstatus`;
CREATE TABLE IF NOT EXISTS `reservationstatus` (
  `status` varchar(32) NOT NULL,
  PRIMARY KEY (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `reservationstatus` (`status`) VALUES
('Canceled'),
('Confirmed'),
('Passed');

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `email` varchar(256) NOT NULL,
  `phone` varchar(16) NOT NULL,
  `address` text NOT NULL,
  `addressCoord` varchar(256) NOT NULL DEFAULT '"[0,0]"',
  `picture` text NOT NULL,
  `password` text NOT NULL,
  `roles` varchar(32) NOT NULL DEFAULT 'particulier',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

INSERT INTO `users` (`id`, `name`, `email`, `phone`, `address`, `addressCoord`, `picture`, `password`, `roles`) VALUES
(1, 'Soat', 'robin.solde.pro@gmail.com', '', '89 Quai Panhard et Levassor, 75013 Paris, France', '0,0', 'http://tfood-server.hd3midcur3.eu-west-1.elasticbeanstalk.com/KRZgDA7spAGVLMGCpL5sO17Plfq3Cbqezgvfs29ixJkY0IUg2nsoat.png', '$2a$10$rXCdjRTuzo5qQqI1TkhEaeORlvPrOu1.r9qFtsbz3oeohRwqDRQB6', 'entreprise'),
(2, 'Google', 'robin.solde@soat.fr', '', '20, N2, 93350 Paris, France', '0,0', 'http://tfood-server.hd3midcur3.eu-west-1.elasticbeanstalk.com/VkHhZricbqQgDmgTzATumEox1Y7PwYjVpdcT7h2eioVoBNOia1kisspng-google-logo-g-suite-google-5ab6f1cee66464.5739288415219388949437.jpg', '$2a$10$LAJnmeIeeR/6oItlqznODe42MoAjtF5up0JJAGi.PekDgVjeXYfsC', 'entreprise'),
(3, 'Facebook', 'robin.solde.scei@gmail.com', '', '6 Rue Ménars, 75002 Paris, France', '0,0', 'http://tfood-server.hd3midcur3.eu-west-1.elasticbeanstalk.com/PlejZXogsex8kCncZJhhBM76qxRam8QwHSPpNwINRcHzwTU4msfacebook.jpg', '$2a$10$lnUieUb9erIgwwVYzx73guQParurLYSQHNdPaEqDSlQUa9h6lBF3.', 'entreprise'),
(4, 'Robin', 'robin.solde@gmail.com', '', '88 Avenue du Président Salvador Allende, 93100 Montreuil, France', '0,0', 'http://48998aac.ngrok.io/AiYQiYcpImFfoJcUSvUm8X2QEipQbtvFjssjB722D0l2vcDoS0image.jpg', '$2a$10$WExYGpiv7uDXyWO5W4ULs.yM3f.17B1f7kgv6inyCNNKvR1hwmnDC', 'particulier'),
(5, 'RestauCharitable', 'bucket2.coc@gmail.com', '', '53 Boulevard Rodin, 92130 Issy-les-Moulineaux, France', '0,0', 'http://48998aac.ngrok.io/NCG9gTyK9DOtAH4FQADo0f51637GpoySx9IGYprwZ7CeBfUEqqassociations.jpg', '$2a$10$JtBIIlU4ddjJKIPWR306qeAQYrIlrA3SQUqBet4Z/Qr6lhEBbAY7a', 'association'),
(6, 'Resto du Coeur', 'vanardois.romain@gmail.com', '', '7-15 Avenue de la Porte de la Villette, 75019 Paris, France', ',', 'http://48998aac.ngrok.io/bIAyJpe9i1xMmmhmmgSbVkrflIArUItmNVIuMR1UylyRdcxF4kLogo_Restos-du-coeur.png', '$2a$10$BOrq5vTyF3XnAQYZRzJQtOAnCbAgA9p5CvgvVfmbJUf4TrC85q57S', 'association');

DROP TABLE IF EXISTS `user_reservation`;
CREATE TABLE IF NOT EXISTS `user_reservation` (
  `idUser` int(11) NOT NULL,
  `idReservation` int(11) NOT NULL,
  PRIMARY KEY (`idUser`,`idReservation`),
  KEY `idReservation` (`idReservation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `notifications`
  ADD CONSTRAINT `notifications_ibfk_1` FOREIGN KEY (`target`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `notifications_ibfk_2` FOREIGN KEY (`author`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `notifications_ibfk_3` FOREIGN KEY (`type`) REFERENCES `notificationstype` (`type`);

ALTER TABLE `offers`
  ADD CONSTRAINT `offers_ibfk_2` FOREIGN KEY (`status`) REFERENCES `offerstatus` (`status`),
  ADD CONSTRAINT `offers_ibfk_3` FOREIGN KEY (`target`) REFERENCES `offertarget` (`target`),
  ADD CONSTRAINT `offers_ibfk_4` FOREIGN KEY (`type`) REFERENCES `offertype` (`type`),
  ADD CONSTRAINT `offers_ibfk_5` FOREIGN KEY (`author`) REFERENCES `users` (`id`);

ALTER TABLE `reservations`
  ADD CONSTRAINT `reservations_ibfk_1` FOREIGN KEY (`offerId`) REFERENCES `offers` (`id`),
  ADD CONSTRAINT `reservations_ibfk_2` FOREIGN KEY (`status`) REFERENCES `reservationstatus` (`status`),
  ADD CONSTRAINT `reservations_ibfk_3` FOREIGN KEY (`userID`) REFERENCES `users` (`id`);

ALTER TABLE `user_reservation`
  ADD CONSTRAINT `user_reservation_ibfk_1` FOREIGN KEY (`idReservation`) REFERENCES `reservations` (`id`),
  ADD CONSTRAINT `user_reservation_ibfk_2` FOREIGN KEY (`idUser`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
