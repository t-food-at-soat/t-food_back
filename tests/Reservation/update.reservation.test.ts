var assert = require('assert');
const expect = require('chai').expect;

import Offer from '../../ts/interfaces/offer.interfaces';
import OfferType from '../../ts/types/offerType.types';
import Point from '../../ts/types/point.types';
import Status from '../../ts/types/status.types';
import OfferRepository from '../../src/repositories/offer.repositoryFake';
import OfferTarget from '../../ts/types/offerTarget.types';
import ReservationRepository from '../../src/repositories/reservation.repositoryFake';
import Reservation from '../../ts/interfaces/reservation.interfaces';
import ReservationStatus from '../../ts/types/reservationStatus.types';


const offerA =  new Offer(
    1,
    "old individual",
    new Date("2042-01-01 00:00"),
    OfferType.Pizza,
    OfferTarget.individual,
    4,
    "53 boulevard Rodin, Issy-les-moulineaux",
    "https://images.unsplash.com/photo-1511690656952-34342bb7c2f2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=600&q=60"
);

const offerB =  new Offer(
    2,
    "new individual",
    new Date("2000-01-01 00:00"),
    OfferType.Pizza,
    OfferTarget.individual,
    4,
    "53 boulevard Rodin, Issy-les-moulineaux",
    "https://images.unsplash.com/photo-1511690656952-34342bb7c2f2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=600&q=60"
);
   
const reservationA = new Reservation(null, offerA, new Date())
reservationA.id = 1;
const reservationB = new Reservation(null, offerB, new Date())
reservationB.id = 2;


describe('Reservation update', function() {
    beforeEach(async function(){
        await ReservationRepository.breakRepo();
        await ReservationRepository.createRepo();   
    })
    afterEach(async function() {
        await  ReservationRepository.breakRepo();
    });

    it("should have a Canceled status", async function(){
        await ReservationRepository.reserveOffer(reservationA);
        const res = await ReservationRepository.cancelReservation(reservationA);
        expect(res.data.status).to.equal(ReservationStatus.Canceled);
    })

    it("should return an error when an unavailable data", async function(){
        await  ReservationRepository.breakRepo();
        const res = await ReservationRepository.cancelReservation(reservationA);
        expect(res.status).to.equal("failed");
    })

    it("should return an error when an unavailable reservation", async function(){
        await ReservationRepository.reserveOffer(reservationA);
        const res = await ReservationRepository.cancelReservation(reservationB);
        expect(res.status).to.equal("failed");
    })

});