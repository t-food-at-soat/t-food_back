const expect = require('chai').expect;

import Offer from '../../ts/interfaces/offer.interfaces';
import OfferType from '../../ts/types/offerType.types';
import Point from '../../ts/types/point.types';
import OfferTarget from '../../ts/types/offerTarget.types';
import ReservationRepository from '../../src/repositories/reservation.repositoryFake';
import Reservation from '../../ts/interfaces/reservation.interfaces';
import ReservationStatus from '../../ts/types/reservationStatus.types';

const offerA =  new Offer(
    1,
    "old individual",
    new Date("2042-01-01 00:00"),
    OfferType.Pizza,
    OfferTarget.individual,
    4,
    "53 boulevard Rodin, Issy-les-moulineaux",
    "https://images.unsplash.com/photo-1511690656952-34342bb7c2f2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=600&q=60"
);

const offerB =  new Offer(
    2,
    "new individual",
    new Date("2000-01-01 00:00"),
    OfferType.Pizza,
    OfferTarget.individual,
    4,
    "53 boulevard Rodin, Issy-les-moulineaux",
    "https://images.unsplash.com/photo-1511690656952-34342bb7c2f2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=600&q=60"
);
   
const reservationA = new Reservation(null, offerA, new Date())
reservationA.id = 1;
const reservationB = new Reservation(null, offerB, new Date())
reservationB.id = 2;



describe('Reeserve offer', function() {
    beforeEach(async function(){
        await ReservationRepository.breakRepo();
        await ReservationRepository.createRepo();   
    })
    afterEach(async function() {
        await  ReservationRepository.breakRepo();
    });

    it("should have a Passed status", async function(){
        await ReservationRepository.reserveOffer(reservationA);
        expect(reservationA.status).to.equal(ReservationStatus.Passed);
    })

    it("should fail when an unvailable data", async function(){
        ReservationRepository.breakRepo();
        const res = await ReservationRepository.reserveOffer(reservationA);
        expect(res.status).to.equal("failed");
    })


});