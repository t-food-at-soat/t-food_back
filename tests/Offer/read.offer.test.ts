var assert = require('assert');
const expect = require('chai').expect;

import Offer from '../../ts/interfaces/offer.interfaces';
import OfferType from '../../ts/types/offerType.types';
import Point from '../../ts/types/point.types';
import Status from '../../ts/types/status.types';
import OfferRepository from '../../src/repositories/offer.repositoryFake';
import OfferTarget from '../../ts/types/offerTarget.types';
import User from '../../ts/interfaces/user.interfaces';
import Roles from '../../ts/types/roles.types';

const oldOffer =  new Offer(
    1,
    "old individual",
    new Date("2042-01-01 00:00"),
    OfferType.Pizza,
    OfferTarget.individual,
    4,
    "53 boulevard Rodin, Issy-les-moulineaux",
    "https://images.unsplash.com/photo-1511690656952-34342bb7c2f2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=600&q=60",
    new User("Soat" ,"exemple@test.com", "********", "0789345643", "address", Roles.INDIVIDUAL)
);

const newOffer =  new Offer(
    2,
    "new individual",
    new Date("2000-01-01 00:00"),
    OfferType.Pizza,
    OfferTarget.individual,
    4,
    "53 boulevard Rodin, Issy-les-moulineaux",
    "https://images.unsplash.com/photo-1511690656952-34342bb7c2f2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=600&q=60",
    new User("Soat" ,"exemple@test.com", "********", "0789345643", "address", Roles.INDIVIDUAL)
);
const middleOffer =  new Offer(
    3,
    "middle individual",
    new Date("2005-01-01 00:00"),
    OfferType.Pizza,
    OfferTarget.individual,
    4,
    "53 boulevard Rodin, Issy-les-moulineaux",
    "https://images.unsplash.com/photo-1511690656952-34342bb7c2f2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=600&q=60",
    new User("psedo" ,"exemple@test.com", "********", "0789345643", "address", Roles.INDIVIDUAL)
);

const validOffer =  new Offer(
    4,
    "valid individual",
    new Date(new Date("2019-09-23 17:30")),
    OfferType.Pizza,
    OfferTarget.individual,
    4,
    "53 boulevard Rodin, Issy-les-moulineaux",
    "https://images.unsplash.com/photo-1511690656952-34342bb7c2f2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=600&q=60"
);
const oldOfferAsso =  new Offer(
    5,
    "old Asso",
    new Date("2042-01-01 00:00"),
    OfferType.Pizza,
    OfferTarget.association,
    4,
    "53 boulevard Rodin, Issy-les-moulineaux",
    "https://images.unsplash.com/photo-1511690656952-34342bb7c2f2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=600&q=60",
    new User("psedo" ,"exemple@test.com", "********", "0789345643", "address", Roles.INDIVIDUAL)
);

const newOfferAsso =  new Offer(
    6,
    "new Asso",
    new Date("2000-01-01 00:00"),
    OfferType.Pizza,
    OfferTarget.association,
    4,
    "53 boulevard Rodin, Issy-les-moulineaux",
    "https://images.unsplash.com/photo-1511690656952-34342bb7c2f2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=600&q=60"
);
const middleOfferAsso =  new Offer(
    7,
    "middle Asso",
    new Date("2005-01-01 00:00"),
    OfferType.Pizza,
    OfferTarget.association,
    4,
    "53 boulevard Rodin, Issy-les-moulineaux",
    "https://images.unsplash.com/photo-1511690656952-34342bb7c2f2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=600&q=60"
);

const validNewOfferAsso =  new Offer(
    8,
    "valid Asso",
    new Date(new Date("2019-09-23 17:30")),
    OfferType.Pizza,
    OfferTarget.association,
    4,
    "53 boulevard Rodin, Issy-les-moulineaux",
    "https://images.unsplash.com/photo-1511690656952-34342bb7c2f2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=600&q=60"
);


describe('Offer read', function() {
    beforeEach(async function(){
        await OfferRepository.breakRepo();
        await OfferRepository.createRepo();
        await OfferRepository.addOffer(validOffer);
        await OfferRepository.addOffer(validNewOfferAsso);
        await OfferRepository.addOffer(middleOffer);
        await OfferRepository.addOffer(middleOfferAsso);
        await OfferRepository.addOffer(newOffer);
        await OfferRepository.addOffer(oldOffer);
        await OfferRepository.addOffer(oldOfferAsso);
        await OfferRepository.addOffer(newOfferAsso);
    })
    afterEach(async function() {
        await  OfferRepository.breakRepo();
    });

    it("should return an array of all individual offers", async function(){
        const res = await OfferRepository.findAllOrdersByTargets(OfferTarget.individual);
        expect(res.data).to.be.an('array');
        res.data.forEach(element => {
            expect(element).to.be.an.instanceOf(Offer)
            expect(element.target).to.equal(OfferTarget.individual)
        });
    })
    it("should return an array of all association offers", async function(){
        const res = await OfferRepository.findAllOrdersByTargets(OfferTarget.association);
        expect(res.data).to.be.an('array');
        res.data.forEach(element => {
            expect(element.target).to.equal(OfferTarget.association)
        });
    })

    it("should return an error when finding all association offers and unavailable data", async function(){
        OfferRepository.breakRepo();
        const res = await OfferRepository.findAllOrdersByTargets(OfferTarget.association);
        expect(res.status).to.equal("failed")
    })

    it("should return the Individual order list sorted by newest", async function(){
        const res = await OfferRepository.findAllOrdersSortedByNewest(OfferTarget.individual)
        expect(res.data[res.data.length - 1]).to.equal(oldOffer)
        res.data.forEach(element => {
            expect(element.target).to.equal(OfferTarget.individual)
        });
        
    })
    it("should return the association order list sorted by newest", async function(){
        const res = await OfferRepository.findAllOrdersSortedByNewest(OfferTarget.association)
        expect(res.data[res.data.length - 1]).to.equal(oldOfferAsso)
        res.data.forEach(element => {
            expect(element.target).to.equal(OfferTarget.association)
        });
        
    })

    it("should return an error when order list sorted by newst and unavailable data", async function(){
        OfferRepository.breakRepo();
        const res = await OfferRepository.findAllOrdersSortedByNewest(OfferTarget.association);
        expect(res.status).to.equal("failed")
    })

    it("should return the Individual orders list sorted by oldest", async function(){
        const res = await OfferRepository.findAllOrdersSortedByOldest(OfferTarget.individual)
        expect(res.data[res.data.length - 1]).to.equal(newOffer)
        res.data.forEach(element => {
            expect(element.target).to.equal(OfferTarget.individual)
        });

    })

    it("should return the Association order list sorted by oldest", async function(){
        const res = await OfferRepository.findAllOrdersSortedByOldest(OfferTarget.association)
        expect(res.data[res.data.length - 1]).to.equal(newOfferAsso)
        res.data.forEach(element => {
            expect(element.target).to.equal(OfferTarget.association)
        });

    })

    it("should return an error when order list sorted by oldest and unavailable data", async function(){
        OfferRepository.breakRepo();
        const res = await OfferRepository.findAllOrdersSortedByOldest(OfferTarget.association);
        expect(res.status).to.equal("failed")
    })

    it("should return the offer with id = 4", async function(){
        const res = await OfferRepository.findOfferById(4);
        expect(res.data).to.equal(validOffer)
    })

    
    it("should return an error when find by id and unavailable data", async function(){
        OfferRepository.breakRepo();
        const res = await OfferRepository.findOfferById(4);
        expect(res.status).to.equal("failed")
    })

    it("should return all offer created by SOAT",async ()=> {
        const offers = await OfferRepository.findAllOrdersByAuthor("Soat");
        offers.data.forEach(element => {
            expect(element.author).to.equal("Soat");
        });
    })

    
    it("should return failed if trying get all offer created by SOAT and data is unavailable",async ()=> {
        OfferRepository.breakRepo();
        const res = await OfferRepository.findAllOrdersByAuthor("Soat");
        expect(res.status).to.equal("failed")
    })


    it("should return an error when looking for reservations by id", async function(){
        const res = await OfferRepository.findOfferById(12);
        expect(res.status).to.equal("failed")
    })

    //TODO Test les distance // GMAP Requis
   
});