var assert = require('assert');
const expect = require('chai').expect;

import Offer from '../../ts/interfaces/offer.interfaces';
import OfferType from '../../ts/types/offerType.types';
import Point from '../../ts/types/point.types';
import Status from '../../ts/types/status.types';
import OfferRepository from '../../src/repositories/offer.repositoryFake';
import OfferTarget from '../../ts/types/offerTarget.types';

const validNewOffer =  new Offer(
    1,
    "Pizza Meetup",
    new Date(new Date("2019-09-23 17:30")),
    OfferType.Pizza,
    OfferTarget.individual,
    4,
    "53 boulevard Rodin, Issy-les-moulineaux",
    "https://images.unsplash.com/photo-1511690656952-34342bb7c2f2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=600&q=60"
);

const validNewOfferAsso =  new Offer(
    1,
    "Pizza Meetup",
    new Date(new Date("2019-09-23 17:30")),
    OfferType.Pizza,
    OfferTarget.association,
    4,
    "53 boulevard Rodin, Issy-les-moulineaux",
    "https://images.unsplash.com/photo-1511690656952-34342bb7c2f2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=600&q=60"
);

describe('Offer Creation', function() {
    beforeEach(async function(){
        await OfferRepository.breakRepo();
        await OfferRepository.createRepo();
    });
    
    afterEach(async function() {
        await OfferRepository.breakRepo();
    });

    it('should return a new Offer empty object', function() {
        expect(new Offer()).to.be.an.instanceof(Offer);
    });
    it('should return a new Offer object', function() {
        expect(validNewOffer).to.be.an.instanceof(Offer);
    });
    it('offer should have an opened status', function() {
        expect(validNewOffer.status).to.equal(Status.Opened);
    });
    it('offer should have a pizza type', function() {
        expect(validNewOffer.type).to.equal(OfferType.Pizza);
    });
    it('latitude should be equal to 0', function() {
        expect(validNewOffer.location).to.equal("53 boulevard Rodin, Issy-les-moulineaux");
    });
    it('target should be association', function() {
        expect(validNewOfferAsso.target).to.equal(OfferTarget.association);
    });
    it('target should be individual', function() {
        expect(validNewOffer.target).to.equal(OfferTarget.individual);
    });
    it('should insert an offer after a push', async function(){
        const offers = await OfferRepository.findAllOrdersByTargets(validNewOffer.target);
        const offerSize = offers.data.length; 
        OfferRepository.addOffer(validNewOffer);
        const newOfferSize = await OfferRepository.findAllOrdersByTargets(validNewOffer.target);
        expect(newOfferSize.data.length).to.equal(offerSize + 1);
    })
    it("should return an error when creating an offer and unavailable data", async function(){
        OfferRepository.breakRepo();
        const res = await OfferRepository.addOffer(validNewOffer);
        expect(res.status).to.equal("failed")
    })
});