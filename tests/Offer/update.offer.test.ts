var assert = require('assert');
const expect = require('chai').expect;

import Offer from '../../ts/interfaces/offer.interfaces';
import OfferType from '../../ts/types/offerType.types';
import Point from '../../ts/types/point.types';
import Status from '../../ts/types/status.types';
import OfferRepository from '../../src/repositories/offer.repositoryFake';
import OfferTarget from '../../ts/types/offerTarget.types';

const oldOffer =  new Offer(
    1,
    "old individual",
    new Date("2042-01-01 00:00"),
    OfferType.Pizza,
    OfferTarget.individual,
    4,
    "53 boulevard Rodin, Issy-les-moulineaux",
    "https://images.unsplash.com/photo-1511690656952-34342bb7c2f2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=600&q=60"
);

const newOffer =  new Offer(
    2,
    "new individual",
    new Date("2000-01-01 00:00"),
    OfferType.Pizza,
    OfferTarget.individual,
    4,
    "53 boulevard Rodin, Issy-les-moulineaux",
    "https://images.unsplash.com/photo-1511690656952-34342bb7c2f2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=600&q=60"
);
const middleOffer =  new Offer(
    3,
    "middle individual",
    new Date("2005-01-01 00:00"),
    OfferType.Pizza,
    OfferTarget.individual,
    4,
    "53 boulevard Rodin, Issy-les-moulineaux",
    "https://images.unsplash.com/photo-1511690656952-34342bb7c2f2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=600&q=60"
);

const validOffer =  new Offer(
    4,
    "valid individual",
    new Date("2019-09-23 17:30"),
    OfferType.Pizza,
    OfferTarget.individual,
    4,
    "53 boulevard Rodin, Issy-les-moulineaux",
    "https://images.unsplash.com/photo-1511690656952-34342bb7c2f2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=600&q=60"
);
const oldOfferAsso =  new Offer(
    5,
    "old Asso",
    new Date("2042-01-01 00:00"),
    OfferType.Pizza,
    OfferTarget.association,
    4,
    "53 boulevard Rodin, Issy-les-moulineaux",
    "https://images.unsplash.com/photo-1511690656952-34342bb7c2f2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=600&q=60"
);

const newOfferAsso =  new Offer(
    6,
    "new Asso",
    new Date("2000-01-01 00:00"),
    OfferType.Pizza,
    OfferTarget.association,
    4,
    "53 boulevard Rodin, Issy-les-moulineaux",
    "https://images.unsplash.com/photo-1511690656952-34342bb7c2f2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=600&q=60"
);
const middleOfferAsso =  new Offer(
    7,
    "middle Asso",
    new Date("2005-01-01 00:00"),
    OfferType.Pizza,
    OfferTarget.association,
    4,
    "53 boulevard Rodin, Issy-les-moulineaux",
    "https://images.unsplash.com/photo-1511690656952-34342bb7c2f2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=600&q=60"
);

const validNewOfferAsso =  new Offer(
    8,
    "valid Asso",
    new Date(new Date("2019-09-23 17:30")),
    OfferType.Pizza,
    OfferTarget.association,
    4,
    "53 boulevard Rodin, Issy-les-moulineaux",
    "https://images.unsplash.com/photo-1511690656952-34342bb7c2f2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=600&q=60"
);


describe('Offer update', function() {
    beforeEach(async function(){
        await OfferRepository.breakRepo();
        await OfferRepository.createRepo();
        await OfferRepository.addOffer(validOffer);
        await OfferRepository.addOffer(validNewOfferAsso);
        await OfferRepository.addOffer(middleOffer);
        await OfferRepository.addOffer(middleOfferAsso);
        await OfferRepository.addOffer(newOffer);
        await OfferRepository.addOffer(oldOffer);
        await OfferRepository.addOffer(oldOfferAsso);
        await OfferRepository.addOffer(newOfferAsso);
    })
    afterEach(async function() {
        await  OfferRepository.breakRepo();
    });

    it("should update the offer description", async function(){        
        let maNouvelleOffre = Object.assign({}, newOffer);
        maNouvelleOffre.description = "Ma nouvelle description";
        const res = await OfferRepository.updateOffer(maNouvelleOffre);
        expect(res.data).to.equal(maNouvelleOffre);
    })

    it("should return failed after trying to update unavailable data", async function(){
        OfferRepository.breakRepo();        
        let maNouvelleOffre = Object.assign({}, newOffer);
        maNouvelleOffre.description = "Ma nouvelle description";
        const res = await OfferRepository.updateOffer(maNouvelleOffre);
        expect(res.status).to.equal("failed");
    })

    it("should return failed after trying to update an undefined offer", async function(){        
        let maNouvelleOffre = Object.assign({}, newOffer);
        maNouvelleOffre.description = "Ma nouvelle description";
        maNouvelleOffre.id = 42;
        const res = await OfferRepository.updateOffer(maNouvelleOffre);
        expect(res.status).to.equal("failed");
    })
});