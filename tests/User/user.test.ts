var assert = require('assert');
const expect = require('chai').expect;

import UserRepository from "../../src/repositories/user.repositoryFake";
import User from "../../ts/interfaces/user.interfaces";
import Roles from "../../ts/types/roles.types";


let userProfile = new User("Soat" ,"exemple@test.com", "********", "0789345643", "address", Roles.INDIVIDUAL);
userProfile.id = 4;

describe('User profile', function() {
    beforeEach(async function(){
        await UserRepository.breakRepo();
        await UserRepository.createRepo();
    })
    
    afterEach(async function() {
        await UserRepository.breakRepo();
    });

    it('should insert an new user', async function() {
        const newUser : User = new User("testName","test@gmail.com", "****", "07453454", "address", Roles.ADMIN);
        const res = await UserRepository.registerUser(newUser);
        expect(res.status).to.equal("success")
    });

    it('should return an error when insert new user and date unavailable', async function() {
        UserRepository.breakRepo();
        const newUser : User = new User("testName","test@gmail.com", "****", "07453454", "address", Roles.ADMIN);
        const res = await UserRepository.registerUser(newUser);
        expect(res.status).to.equal("failed")
    });
    
    it('should return all users', async function() {
        const res = await UserRepository.getAllUsers();
        expect(res.data.length).to.equal(1)
        expect(res.status).to.equal("success")
    });

    it('should return an error when data not available', async function() {
        UserRepository.breakRepo();
        const res = await UserRepository.getAllUsers();
        expect(res.status).to.equal("failed")
    });

    it('should return the user\'s profile info', async function() {
        const res = await UserRepository.getProfileInfoById(4);
        expect(res.data.name).to.equal(userProfile.name)
        expect(res.status).to.equal("success")
    });

    it('should return an error when data unavailable', async function() {
        UserRepository.breakRepo();
        const res = await UserRepository.getProfileInfoById(4);
        expect(res.status).to.equal("failed")
    });

    it('should return an error when unvailable data', async function() {
        UserRepository.breakRepo();
        const res = await UserRepository.editProfileInfo(userProfile);
        expect(res.status).to.equal("failed")
    });

    it('should return an error when unvailable profile', async function() {
        const res = await UserRepository.editProfileInfo(new User("Soat", "exemple-unknown@test.com", "********", "0789345643", "address", Roles.INDIVIDUAL));
        expect(res.status).to.equal("failed")
    });

    it('should return an error when inexist', async function() {
        const res = await UserRepository.getProfileInfoByEmail("exemple-unknown@test.com");
        expect(res.status).to.equal("failed")
        expect(res.statusCode).to.equal(404)
    });

    it('should return the user by email', async function() {
        const res = await UserRepository.getProfileInfoByEmail("exemple@test.com");
        expect(res.status).to.equal("success")
        expect(res.statusCode).to.equal(200)
    });

    it('should return an error when get user and unavailable data', async function() {
        UserRepository.breakRepo();
        const res = await UserRepository.getProfileInfoByEmail("exemple@test.com");
        expect(res.status).to.equal("failed")
        expect(res.statusCode).to.equal(500)
    });

    it('should return an error when get user and unavailable data', async function() {
        UserRepository.breakRepo();
        const res = await UserRepository.getProfileInfoByEmail("exemple@test.com");
        expect(res.status).to.equal("failed")
        expect(res.statusCode).to.equal(500)
    });

    it('should return an error when unavailable user', async function() {
        userProfile.email = "exemple-unknown@test.com";
        const res = await UserRepository.editProfileInfo(userProfile);
        expect(res.status).to.equal("failed")
        expect(res.statusCode).to.equal(404)
    });

    it('should return an error when unavailable data', async function() {
        UserRepository.breakRepo();
        const res = await UserRepository.editProfileInfo(userProfile);
        expect(res.status).to.equal("failed")
        expect(res.statusCode).to.equal(500)
    });
    
    it('should update the user profile', async function() {
        userProfile.email = "exemple@test.com";
        const res = await UserRepository.editProfileInfo(userProfile);
        expect(res.status).to.equal("success")
        expect(res.statusCode).to.equal(200)
    });

});