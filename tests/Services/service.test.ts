import Service from "../../src/utils/service";
import HTTP_METHODS from "../../ts/types/httpMethod.types";
import { getOfferSchema } from "../../src/services/Offer/offer.schema";
import Roles from "../../ts/types/roles.types";

const expect = require("chai").expect;
import * as request from "supertest";
import app from "../../src/main";
import Auth from "../../src/utils/auth";
import { log } from "util";

describe("Service Utils", () => {
  it("should throw an error if initRoutes is not overrides", () => {
    class MyNewService extends Service {}
    expect(() => new MyNewService()).to.throw();
  });

  it("should return an unauthorized error if no token is specified in the header and the route is not public", () => {
    request(app)
      .get("/offers/admin")
      .end(function(err, res) {
        expect(res.statusCode).to.equal(401);
      });
  });

  // it("should access route if jwt role is an authorized role", (done) => {

  //     const token = Auth.sign({
  //         "name": "John Doe",
  //         "roles" : "admin",
  //       })

  //     request(app)
  //         .get('/offers/admin')
  //         .set('Authorization', token)
  //         .expect(200)
  //         .end(function (err, res) {
  //             console.log(res)
  //             if (err) return done(err);
  //             done();
  //         });
  // })

  it("should not acces route if jwt role is an unauthorized role", done => {
    const token = Auth.sign({
      name: "John Doe",
      roles: "particulier"
    });

    request(app)
      .get("/offers/admin")
      .set("Authorization", token)
      .expect(401)
      .end(function(err, res) {
        done();
      });
  });

  // it("should not access route if jwt is expired", (done) => {

  //       const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiSm9obiBEb2UiLCJyb2xlcyI6ImFkbWluIiwiaWF0IjoxNTY5MzQwMTg0LCJleHAiOjE1NjkzNTQ1ODR9.-6_GgcEsXtC8ydDra50b8W4OHQaCPLnsKgI23_IU01o";

  //     request(app)
  //         .get('/offers/admin')
  //         .set('Authorization', token)
  //         .expect(401)
  //         .end(function (err, res) {
  //             expect(res.text).to.equal('{"code":4012,"message":"Le token est expiré"}')
  //             if (err) return done(err);
  //             done();
  //         });
  // })

  it("should not access route if jwt is no valid", done => {
    const token = "toto";

    request(app)
      .get("/offers/admin")
      .set("Authorization", token)
      .expect(401)
      .end(function(err, res) {
        expect(res.text).to.equal(
          '{"code":4013,"message":"Le mot de passe et l\'identifiant utilisateur ne correspondent pas"}'
        );
        if (err) return done(err);
        done();
      });
  });
});
