const expect = require('chai').expect;
import * as request from "supertest";
import app from '../../src/main'
import { assert } from "chai";
import OfferRepository from "../../src/repositories/offer.repositoryFake";

describe('Offer service', () => {
    beforeEach(() => {
        OfferRepository.createRepo();
    })

    // les tests des routes sont desactivé pour le moment le temps de mettre en place la bdd

    // it("should return a array of offer", async () => {
    //     request(app).get('/offers')
    //         .end(function (err, res) {
    //             expect(res.statusCode).to.equal(200);
    //             expect(res.body).to.be.an("object");
    //             expect(res.body.status).to.equal("success");
    //         });
    // })

   

    // it("should return a http error 404 if page route is unknown", async () => {
    //     request(app).get('/oofffrree')
    //         .end(function (err, res) {
    //             expect(res.statusCode).to.equal(404);
    //         });
    // })

    // it("should add an offer to data", async () => {
    //     request(app).post('/createOffer')
    //         .send({
    //             "id": 999,
    //             "description": "Une très jolie description d'offre",
    //             "datetime": "2019-09-21 10:23",
    //             "type": "Pizza",
    //             "quantity": "42",
    //             "location": [45, 6],
    //             "picture": "https://images.unsplash.com/photo-1534308983496-4fabb1a015ee?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=600&q=60"
    //         }
    //         )
    //         .end(async function (err, res) {
    //             expect(res.statusCode).to.equal(200);
    //             expect(res.body.status).to.equal("success");
    //             const req = await OfferRepository.findOfferById(999);
    //             expect(req.data.description).to.equal("Une très jolie description d'offre");
    //         });
    // })
    // it("should return an error if body data is not valid", async () => {
    //     request(app).post('/createOffer')
    //         .send({
    //             "id": 999,
    //             "description": 500,
    //             "datetime": "2019-09-21 10:23",
    //             "type": "Pizza",
    //             "quantity": "42",
    //             "location": [45, 6],
    //             "picture": "https://images.unsplash.com/photo-1534308983496-4fabb1a015ee?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=600&q=60"
    //         }
    //         )
    //         .end(function (err, res) {
    //             expect(res.statusCode).to.equal(500);
    //         });
    // })

    // it("should return an error if data is unavailable", async () => {
    //     OfferRepository.breakRepo();
    //     request(app).post('/createOffer')
    //         .send({
    //             "id": 999,
    //             "description": "un desc",
    //             "datetime": "2019-09-21 10:23",
    //             "type": "Pizza",
    //             "quantity": "42",
    //             "location": [45, 6],
    //             "picture": "https://images.unsplash.com/photo-1534308983496-4fabb1a015ee?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=600&q=60"
    //         }
    //         )
    //         .end(function (err, res) {
    //             expect(res.statusCode).to.equal(500);
    //             expect(res.body.status).to.equal("failed");
    //         });
    // })
})