const expect = require('chai').expect;
import * as request from "supertest";
import app from '../../src/main'
import ReservationRepository from "../../src/repositories/reservation.repositoryFake";
import OfferRepository from "../../src/repositories/offer.repositoryFake";
import Offer from "../../ts/interfaces/offer.interfaces";
import OfferTarget from "../../ts/types/offerTarget.types";
import OfferType from "../../ts/types/offerType.types";
import Point from "../../ts/types/point.types";
import Reservation from "../../ts/interfaces/reservation.interfaces";


const offerA =  new Offer(
    1,
    "old individual",
    new Date("2042-01-01 00:00"),
    OfferType.Pizza,
    OfferTarget.individual,
    4,
    "53 boulevard Rodin, Issy-les-moulineaux",
    "https://images.unsplash.com/photo-1511690656952-34342bb7c2f2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=600&q=60"
);

const reservationA = new Reservation(null, offerA, new Date())
reservationA.id = 1;

describe('Reservations service', () => {
    beforeEach(() => {
        OfferRepository.createRepo();
        ReservationRepository.createRepo();
    })
    // it("should return a array of reservation", async () => {
    //     request(app).get('/reservations')
    //         .end(function (err, res) {
    //             expect(res.statusCode).to.equal(200);
    //             expect(res.body).to.be.an("object");
    //             expect(res.body.status).to.equal("success");
    //         });
    // })

    // it("should return a error if data is unavailable", () => {
    //     ReservationRepository.breakRepo();
    //     request(app).get('/reservations')
    //         .end(function (err, res) {
    //             expect(res.statusCode).to.equal(500);
    //             expect(res.body).to.be.an("object");
    //             expect(res.body.status).to.equal("failed");
    //         });
    // })

    // it("should return a http error 404 if page route is unknown", async () => {
    //     request(app).get('/reservationss')
    //         .end(function (err, res) {
    //             expect(res.statusCode).to.equal(404);
    //         });
    // })

    
    // it("should return an error when offer not found", async () => {
    //     request(app).post('/reserveOffer')
    //         .send({"id": 1})
    //         .end(function (err, res) {
    //             expect(res.statusCode).to.equal(404);
    //             expect(res.body.status).to.equal("failed");
    //         });
    // })

    // it("should return an error when offer not found", async () => {
    //     await OfferRepository.addOffer(offerA);
    //     request(app).post('/reserveOffer')
    //         .send({"id": 1})
    //         .end(function (err, res) {
    //             expect(res.statusCode).to.equal(200);
    //             expect(res.body.status).to.equal("success");
    //         });
    // })

    // it("should return an error when reservation not found", async () => {
    //     request(app).put('/cancelReservation')
    //         .send({"id": 1})
    //         .end(function (err, res) {
    //             expect(res.statusCode).to.equal(404);
    //             expect(res.body.status).to.equal("failed");
    //         });
    // })

    // it("should return an error when reservation not found", async () => {
    //     await OfferRepository.addOffer(offerA);
    //     const result = await ReservationRepository.reserveOffer(reservationA);
    //     request(app).put('/cancelReservation')
    //         .send({"id": 1})
    //         .end(function (err, res) {
    //             expect(res.statusCode).to.equal(200);
    //             expect(res.body.status).to.equal("success");
    //         });
    // })

    // it("should return a reservation of the specified id", async () => {
    //     await OfferRepository.addOffer(offerA);
    //     await ReservationRepository.reserveOffer(reservationA);
    //     request(app).put('/getReservation/1')
    //         .send({"id": 1})
    //         .end(function (err, res) {
    //             expect(res.statusCode).to.equal(200);
    //             expect(res.body.status).to.equal("success");
    //         });
    // })

    // it("should return an error when reservation not found", async () => {
    //     request(app).put('/getReservation/1')
    //         .send({"id": 1})
    //         .end(function (err, res) {
    //             expect(res.statusCode).to.equal(404);
    //             expect(res.body.status).to.equal("failed");
    //         });
    // })



})